package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;
import com.itau.geometria.formas.*;

public class App {
	public static void main(String[] args) {
		int lados = 5;
		int altura = 2;
		int largura = 2;
		int ladoA = 4;
		int ladoB = 4;
		int ladoC = 2;
		
		InterfaceUsuario interfaceUsuario = new InterfaceUsuario();
		List<Double> listaDigitados = new ArrayList<>();
		listaDigitados = interfaceUsuario.recebeInformcoes(3);
		
     	Circulo circulo = new Circulo(lados);
		double area = circulo.calcularArea();
		System.out.println(area);

		Retangulo retangulo = new Retangulo(altura, largura);
		double areaRetangulo = retangulo.calcularArea();
		System.out.println(areaRetangulo);

		try {
			Triangulo triangulo = new Triangulo(ladoA, ladoB, ladoC);
			double areaTriangulo = triangulo.calcularArea();
			System.out.println(areaTriangulo);
		} catch (Exception e) {
		}

	}
}
