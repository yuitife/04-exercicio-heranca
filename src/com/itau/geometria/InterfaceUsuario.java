package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InterfaceUsuario {
	
	public List<Double> recebeInformcoes(int vezes) {
		double numeroDigitado = 0;
		List<Double> listaDigitados = new ArrayList<>();
		
		for (int i=0; i<vezes; i++) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Digite o Lado ou 0 para terminar: ");
				numeroDigitado = scanner.nextDouble();
				if (numeroDigitado != 0) {
					listaDigitados.add(numeroDigitado);
				} else {
					i=vezes;
				}
			
		}
		return listaDigitados;
	}
}
