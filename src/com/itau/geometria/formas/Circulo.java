package com.itau.geometria.formas;

public class Circulo extends Forma{
	int raio;
	
	@Override
	public double calcularArea() {
		return Math.pow(raio, 2) * Math.PI;
	}
	
	public Circulo(int raio) {
		this.raio = raio;
	}

}
