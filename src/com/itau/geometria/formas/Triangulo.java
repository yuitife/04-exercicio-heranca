package com.itau.geometria.formas;

public class Triangulo extends Forma {
	int ladoA;
	int ladoB;
	int ladoC;

	@Override
	public double calcularArea() {
		double s = (ladoA + ladoB + ladoC) / 2;       
		return Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
	}

	public Triangulo(int ladoA, int ladoB, int ladoC) throws Exception {
		if ((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA == true) {
			this.ladoA = ladoA;
			this.ladoB = ladoB;
			this.ladoC = ladoC;
		} else {
			System.out.println("Lados não formam um triangulo");
			throw new Exception("Lados não formam um triangulo");
		}
	}
}
