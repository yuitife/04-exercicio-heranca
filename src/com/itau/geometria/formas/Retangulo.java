package com.itau.geometria.formas;

public class Retangulo extends Forma{
	private int largura;
	private int altura;
	
	@Override
	public double calcularArea() {
		return altura * largura;
	}
	
	public Retangulo(int altura, int largura) {
		this.largura = largura;
		this.altura = altura;
	}

	public int getLargura() {
		return largura;
	}

	public int getAltura() {
		return altura;
	}
	
}
